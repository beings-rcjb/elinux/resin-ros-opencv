#!/bin/bash

die() {
	ra=$1
	shift
	echo "$*" >&2
	exit ${ra}
}

arch=$1 # hardware architecture or board
tag=$2
IFS=_ read -ra _tagparts <<< ${tag}

ros_tag=${_tagparts[0]}
cv_version=${_tagparts[1]}

case "$arch" in
	"armv7hf")
		perf_opts='-D ENABLE_VFPV3=ON -D ENABLE_NEON=ON'
		;;
esac

sed \
	-e "s@#{ARCH}@${arch}@g" \
    -e "s@#{ROS_TAG}@${ros_tag}@g" \
	-e "s@#{CV_VERSION}@${cv_version}@g" \
	-e "s@#{PERF_OPTS}@${perf_opts}@g" \
	Dockerfile.tpl > Dockerfile.${tag}

docker build --pull \
	-f Dockerfile.${tag} -t ${arch}-ros-opencv:${tag} \
	$(dirname $0)

# TODO: Make optional
rm Dockerfile.${tag}
