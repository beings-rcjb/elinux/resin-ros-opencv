FROM quay.io/ethanwu10/#{ARCH}-ros-numpy:#{ROS_TAG} as builder

# Keep at top for caching
RUN apt-get update && \
    apt-get install -y \
            build-essential cmake pkg-config libjpeg-dev libpng12-dev \
            libtiff5-dev libjasper-dev libavcodec-dev libavformat-dev \
            libswscale-dev libv4l-dev libxvidcore-dev libx264-dev     \
            libatlas-base-dev gfortran \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /build/
WORKDIR /build/
ENV OPENCV_VERSION="#{CV_VERSION}"
RUN curl https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.tar.gz \
         -sSLo opencv.tar.gz && \
    tar -xzf opencv.tar.gz && \
    curl https://github.com/opencv/opencv_contrib/archive/${OPENCV_VERSION}.tar.gz \
         -sSLo opencv_contrib.tar.gz && \
    tar -xzf opencv_contrib.tar.gz
RUN mkdir opencv-${OPENCV_VERSION}/build
WORKDIR opencv-${OPENCV_VERSION}/build
RUN cmake \
          -D WITH_CUDA=OFF \
          -D WITH_TBB=ON \
          -D WITH_V4L=ON \
          -D BUILD_WITH_DEBUG_INFO=OFF \
          -D BUILD_TESTS=OFF \
          -D BUILD_PERF_TESTS=OFF \
          -D BUILD_EXAMPLES=OFF \
          #{PERF_OPTS} \
          -D CMAKE_BUILD_TYPE=Release \
          -D CMAKE_INSTALL_PREFIX=/build/output \
          -D OPENCV_EXTRA_MODULES_PATH="/build/opencv_contrib-${OPENCV_VERSION}/modules" \
          ..
RUN make -j $(nproc)
RUN make install

FROM quay.io/ethanwu10/#{ARCH}-ros-numpy:#{ROS_TAG}

# Keep at top for caching
RUN apt-get update && \
    apt-get install -y \
            build-essential cmake pkg-config libjpeg-dev libpng12-dev \
            libtiff5-dev libjasper-dev libavcodec-dev libavformat-dev \
            libswscale-dev libv4l-dev libxvidcore-dev libx264-dev     \
            libatlas-base-dev gfortran \
    && rm -rf /var/lib/apt/lists/*

COPY --from=builder /build/output /usr/local

RUN ldconfig
